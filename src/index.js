const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

// settings
app.set('port',process.env.PORT || 3000);
mongoose.connect('mongodb://localhost/usuarios',{
  useNewUrlParser: true
})
.then(db => console.log('BD is conected'))
.catch(err => console.log(err));

// midlewares
app.use(morgan('dev'))
app.use(express.json())

// routes
app.use('/api/users', require('./routes/users'));

// static files
app.use(express.static(__dirname + '/public'));

app.listen(app.get('port'),()=>{
  console.log('Server start on port: '+ app.get('port'));
})
