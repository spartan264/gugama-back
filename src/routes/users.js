const express = require('express');
const router = express.Router();

const User = require('../models/User');


// Method : GET
/*
return all users
*/
router.get('/', async (req,res) => {
  const users = await User.find();
  res.json(users);
});
/*
return oune user by id
*/
router.get('/:id', async (req,res) => {
  await User.findById(req.params.id)  
  .exec(function(err,user) {
    res.json(user);
  });
});
/*
return une id by username
*/
router.get('/username/:id', async (req,res) => {
  await User.findOne({username: req.params.id},'_id',function (err,id) {
    res.json(id);
  });
});
/*
return one id by email
*/
router.get('/email/:id', async (req,res) => {
  await User.findOne({email: req.params.id}, '_id',function(err,id) {
    res.json(user);
  });
});
// Method: POST
/*
create a new User
*/
router.post('/', async (req,res) => {
  const user = new User(req.body);
  await user.save();
  res.json({
    state: "User Saved"
  });
});

// Method: PUT
/*
updated User
*/
router.put('/:id', async (req,res) =>{
  await User.findByIdAndUpdate(req.params.id)
  res.json({
    status: "User updated"
  })
});

// Method: DELETE
router.delete('/:id', async (req,res) => {
  await User.findByIdAndRemove(req.params.id);
  res.json({
    state: "User Delete"
  })
});


module.exports = router;
