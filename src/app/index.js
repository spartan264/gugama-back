import Vue from 'vue';

// COMPONENTS ---
/* COMPONENTS APP*/ import App from './App.vue';

new Vue({
  render: h => h(App)
}).$mount('#app')
